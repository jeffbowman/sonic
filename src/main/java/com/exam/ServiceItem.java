package com.exam;

import java.math.BigDecimal;

public class ServiceItem extends OrderItem {

    public ServiceItem(final Item item, final int qt) {
        super(item, qt);
    }

    public BigDecimal getCost(final BigDecimal taxRate) {
        return getItem().getPrice().multiply(new BigDecimal(getQt()));
    }
}
