package com.exam;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * Represents and Order that contains a collection of items.
 *
 * care should be taken to ensure that this class is immutable since it is sent
 * to other systems for processing that should not be able * to change it in any
 * way.
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: Exams are us</p>
 *
 * @author Joe Blow
 *
 * @version 1.0
 *
 */
public class Order implements Serializable {

    private static final long serialVersionUID = -957562155629299044L;
    private static final Comparator<OrderItem> alphaComparator = new Comparator<OrderItem>() {
        public int compare(OrderItem o1, OrderItem o2) {
            return o1.getItem().getName().toLowerCase().compareTo(o2.getItem().getName().toLowerCase());
        }
    };
    private OrderItem[] orderItems;

    public Order(final OrderItem[] orderItems) {
        this.orderItems = orderItems;
    }

    /**
     * Returns the total order cost after the tax has been applied
     */
    public BigDecimal getOrderTotal(final BigDecimal taxRate) {
        BigDecimal total = new BigDecimal(BigInteger.ZERO, 3);
        for (final OrderItem oi : orderItems) {
            total = total.add(oi.getCost(taxRate));
        }
        return total.setScale(2, BigDecimal.ROUND_UP);
    }

    /**
     * Returns a Collection of all items sorted by item name
     *
     * (case-insensitive).
     *
     * @return Collection
     */
    public Collection getItems() {
        final List<OrderItem> items = Arrays.asList(orderItems);
        Collections.sort(items, alphaComparator);
        return items;
    }
}
