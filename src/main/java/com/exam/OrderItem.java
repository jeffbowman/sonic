package com.exam;

import java.math.BigDecimal;

/**
 *
 * Represents an Order Item
 *
 * Abstraction forces one of the subclasses to be used, enabling polymorphism
 * for faster processing.
 *
 * @author David Emler
 *
 * @version 1.0
 *
 */
public abstract class OrderItem {

    private Item item;
    private int qt;

    public OrderItem(final Item item, final int qt) {
        this.item = item;
        this.qt = qt;
    }

    public Item getItem() {
        return item;
    }

    public int getQt() {
        return qt;
    }

    abstract BigDecimal getCost(final BigDecimal taxRate);
}
