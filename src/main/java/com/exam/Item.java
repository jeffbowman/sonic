package com.exam;

import java.math.BigDecimal;

/**
 * Represents a part or service that can be sold.
 *
 * care should be taken to ensure that this class is immutable since it is sent
 * to other systems for processing that should not be able to change it in any
 * way.
 *
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Exams are us</p>
 *
 * @author Joe Blow
 * @version 1.0
 */
public final class Item implements Comparable<Item> {

    private Integer key;
    private String name;
    private BigDecimal price;

    public Item(final Integer key, final String name, final BigDecimal price) {
        this.key = key;
        this.name = name;
        this.price = price;
    }

    public Integer getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public boolean equals(final Object other) {
        if (other == null) {
            return false;
        }

        if (this == other) {
            return true;
        }

        if (other.getClass().isAssignableFrom(Item.class)) {
            final Item item = (Item) other;

            if (key != null && item.getKey() != null
                    && key.intValue() != item.getKey().intValue()) {
                return false;
            }

            if (name != null && item.getName() != null
                    && !name.equals(item.getName())) {
                return false;
            }

            if (!price.equals(item.getPrice())) {
                return false;
            }

            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (key != null ? key.hashCode() : 0);
        hash = 41 * hash + (name != null ? name.hashCode() : 0);
        hash = 41 * hash + Float.floatToIntBits(price.floatValue());
        return hash;
    }

    public int compareTo(final Item other) {
        if (key != other.getKey()) {
            return (key - other.getKey() < 0) ? -1 : 1;
        }
        if (!name.equals(other.getName())) {
            return (name.compareTo(other.getName()));
        }

        return price.compareTo(other.getPrice());
    }
}
