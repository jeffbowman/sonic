package com.exam;

import java.math.BigDecimal;

public class MaterialItem extends OrderItem {

    public MaterialItem(final Item item, final int qt) {
        super(item, qt);
    }

    public BigDecimal getCost(final BigDecimal taxRate) {
        final BigDecimal grossCost = getItem().getPrice().multiply(new BigDecimal(getQt()));
        return grossCost.add(grossCost.multiply(taxRate));
    }
}
