package com.exam.scala

abstract class OrderItem(val item: Item, val qt: Int) {
  def getCost(taxRate: BigDecimal): BigDecimal
}

class ServiceItem(override val item: Item, override val qt: Int) extends OrderItem(item, qt) {
  override def getCost(taxRate: BigDecimal): BigDecimal = {
    item.price * qt
  }
}

class MaterialItem(override val item: Item, override val qt: Int) extends OrderItem(item, qt) {
  override def getCost(taxRate: BigDecimal): BigDecimal = {
    val grossCost = item.price * qt
    grossCost + grossCost * taxRate;
  }
}
