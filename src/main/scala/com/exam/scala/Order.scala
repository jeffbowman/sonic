package com.exam.scala

class Order(val items: Seq[OrderItem]) {
  def getOrderTotal(taxRate: BigDecimal): BigDecimal = {
    items.map(_.getCost(taxRate)).foldLeft(BigDecimal(0.0f))(_ + _)
  }

  def getItems() : Seq[OrderItem] = {
    items.sortWith(_.item.name.toLowerCase < _.item.name.toLowerCase)
  }
}
