package com.exam.scala

case class Item(val key: Int, val name: String, val price: BigDecimal) extends Ordered[Item] {

  def compare(other: Item): Int = {
    if (key != other.key) {
      return if ((key - other.key) < 0) -1 else 1
    }

    if (name != other.name) {
      return if (name < other.name) -1 else 1
    }

    if (price != other.price) {
      return if ((price - other.price) < 0) -1 else 1
    }

    return 0
  }
}
