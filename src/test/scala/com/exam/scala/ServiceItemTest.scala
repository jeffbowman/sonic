package com.exam.scala

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ServiceItemTest extends FunSuite {

  test ("get cost should return the price of the item") {
    val serviceItem = new ServiceItem(new Item(1, "name", 1.0f), 1)
    assert(serviceItem.getCost(1.0f) === 1.0f)
    assert(serviceItem.getCost(3.14159f) === 1.0f)
  }

  test ("get cost should account for quantity") {
    val item = new ServiceItem(new Item(1, "name", 1.0f), 3)
    assert(item.getCost(0.0f) === 3.0f)
    assert(item.getCost(1.0f) === 3.0f)
  }

}
