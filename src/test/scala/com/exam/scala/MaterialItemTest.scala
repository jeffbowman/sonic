package com.exam.scala

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MaterialItemTest extends FunSuite {

  test ("get cost should return correctly calculated cost based on tax") {
    val item = new MaterialItem(new Item(1, "name", 1.0f), 1)
    assert(item.getCost(0.0f) === 1.0)
    assert(item.getCost(0.5f) === 1.5)
    assert(item.getCost(1.0f) === 2.0)
    assert(item.getCost(2.0f) === 3.0)
  }

  test ("get cost should account for quantity") {
    val item = new MaterialItem(new Item(1, "name", 1.0f), 3)
    assert(item.getCost(0.0f) === 3.0)
    assert(item.getCost(1.0f) === 6.0)
  }
}
