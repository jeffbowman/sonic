package com.exam.scala

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ItemTest extends FunSuite {

  test ("equals should return false when null is presented") {
    val i = new Item(1, "name", 0.0f)
    val j = null
    assert(i != j)
  }

  test ("equals should return false when other key is different") {
    val i = new Item(1, "name", 0.0f)
    val j = new Item(2, "name", 0.0f)
    assert(i != j)
  }

  test ("equals should return false when other name is different") {
    val i = new Item(1, "name", 0.0f)
    val j = new Item(1, "other", 0.0f)
    assert(i != j)
  }

  test ("equals should return false when other price is different") {
    val i = new Item(1, "name", 0.0f)
    val j = new Item(1, "name", 0.1f)
    val k = new Item(1, "name", 0.099f)
    assert(i != j)
    assert(i != k)
    assert(j != k)
  }

  test ("equals should return true when other is same by identity") {
    val i = new Item(1, "name", 0.0f)
    val j = i
    assert(i === j)
    assert(i === j)
  }

  test ("equals should return true when other is similar") {
    val i = new Item(1, "name", 0.0f)
    val j = new Item(1, "name", 0.0f)
    assert(i === j)
  }

  test ("compare should return zero when other is same by idenity") {
    val i = new Item(1, "name", 0.0f)
    val j = i
    assert(i.compare(j) == 0)
  }

  test ("compare to should return zero when other is similar"){
    val i = new Item(1, "name", 0.0f)
    val j = new Item(1, "name", 0.0f)
    assert(i.compare(j) == 0)
  }

  test ("compare to should return positive number when other is less"){
    val i = new Item(1, "name", 1.0f)
    val j = new Item(1, "name", 0.1f)
    val k = new Item(1, "fame", 1.0f)
    val l = new Item(0, "name", 1.0f)
    assert(i.compare(j) > 0)
    assert(i.compare(k) > 0)
    assert(i.compare(l) > 0)
  }

  test ("compare to should return negative number when other is more") {
    val i = new Item(1, "name", 1.0f)
    val j = new Item(1, "name", 0.1f)
    val k = new Item(1, "fame", 1.0f)
    val l = new Item(0, "name", 1.0f)
    assert(j.compare(i) < 0)
    assert(k.compare(i) < 0)
    assert(l.compare(i) < 0)
  }
}
