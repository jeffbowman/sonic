package com.exam.scala

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class OrderTest extends FunSuite {

    test ("get order total should return correct sum") {
        val orderItems = new Array[OrderItem](2)
        orderItems(0) = new MaterialItem(new Item(1, "name", 1.0f), 1)
        orderItems(1) = new ServiceItem(new Item(1, "name", 1.0f), 1)
        val order = new Order(orderItems)

        assert(order.getOrderTotal(0.0f) === 2.0)
        assert(order.getOrderTotal(0.5f) === 2.5)
        assert(order.getOrderTotal(1.0f) === 3.0)
        assert(order.getOrderTotal(2.0f) === 4.0)
    }

    test ("get items returns an alphabetically sorted by item name collection") {
        val orderItems = new Array[OrderItem](7)
        orderItems(0) = new MaterialItem(new Item(1, "hamburger", 0.0f), 1)
        orderItems(1) = new MaterialItem(new Item(1, "french fries", 0.0f), 1)
        orderItems(2) = new MaterialItem(new Item(1, "coke", 0.0f), 1)
        orderItems(3) = new MaterialItem(new Item(1, "shake", 0.0f), 1)
        orderItems(4) = new MaterialItem(new Item(1, "Blast", 0.0f), 1)
        orderItems(5) = new MaterialItem(new Item(1, "burrito", 0.0f), 1)
        orderItems(6) = new MaterialItem(new Item(1, "Coney", 0.0f), 1)
        val order = new Order(orderItems)
        val sortedItems = order.getItems()

        assert(sortedItems(0).item.name == "Blast")
        assert(sortedItems(1).item.name == "burrito")
        assert(sortedItems(2).item.name == "coke")
        assert(sortedItems(3).item.name == "Coney")
        assert(sortedItems(4).item.name == "french fries")
        assert(sortedItems(5).item.name == "hamburger")
        assert(sortedItems(6).item.name == "shake")
    }
}
