package com.exam;

import java.math.BigDecimal;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class ItemTest {

    @Test
    public void equalsShouldReturnFalseWhenNullIsPresented() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = null;
        assertThat(i.equals(j), is(false));
    }

    @Test
    public void equalsShouldReturnFalseWhenOtherKeyIsDifferent() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = new Item(2, "name", BigDecimal.ZERO);
        assertThat(i.equals(j), is(false));
    }

    @Test
    public void equalsShouldReturnFalseWhenOtherNameIsDifferent() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = new Item(1, "other", BigDecimal.ZERO);
        assertThat(i.equals(j), is(false));
    }

    @Test
    public void equalsShouldReturnFalseWhenOtherPriceIsDifferent() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = new Item(1, "name", new BigDecimal(0.1f));
        final Item k = new Item(1, "name", new BigDecimal(0.099f));
        assertThat(i.equals(j), is(false));
        assertThat(i.equals(k), is(false));
        assertThat(j.equals(k), is(false));
    }

    @Test
    public void equalsShouldReturnTrueWhenOtherIsSameByIdentity() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = i;
        assertThat(i.equals(j), is(true));
        assertThat(i == j, is(true));
    }

    @Test
    public void equalsShouldReturnTrueWhenOtherIsSimilar() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = new Item(1, "name", BigDecimal.ZERO);
        assertThat(i.equals(j), is(true));
    }

    @Test
    public void compareToShouldReturnZeroWhenOtherIsSameByIdenity() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = i;
        assertThat(i.compareTo(j), is(0));
    }

    @Test
    public void compareToShouldReturnZeroWhenOtherIsSimilar() {
        final Item i = new Item(1, "name", BigDecimal.ZERO);
        final Item j = new Item(1, "name", BigDecimal.ZERO);
        assertThat(i.compareTo(j), is(0));
    }

    @Test
    public void compareToShouldReturnPositiveNumberWhenOtherIsLess() {
        final Item i = new Item(1, "name", BigDecimal.ONE);
        final Item j = new Item(1, "name", new BigDecimal(0.1f));
        final Item k = new Item(1, "fame", BigDecimal.ONE);
        final Item l = new Item(0, "name", BigDecimal.ONE);
        assertThat(i.compareTo(j) > 0, is(true));
        assertThat(i.compareTo(k) > 0, is(true));
        assertThat(i.compareTo(l) > 0, is(true));
    }

    @Test
    public void compareToShouldReturnNegativeNumberWhenOtherIsMore() {
        final Item i = new Item(1, "name", BigDecimal.ONE);
        final Item j = new Item(1, "name", new BigDecimal(0.1f));
        final Item k = new Item(1, "fame", BigDecimal.ONE);
        final Item l = new Item(0, "name", BigDecimal.ONE);
        assertThat(j.compareTo(i) < 0, is(true));
        assertThat(k.compareTo(i) < 0, is(true));
        assertThat(l.compareTo(i) < 0, is(true));
    }
}
