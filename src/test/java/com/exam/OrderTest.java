package com.exam;

import java.math.BigDecimal;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class OrderTest {

    @Test
    public void getOrderTotalShouldReturnCorrectSum() {
        final OrderItem[] orderItems = new OrderItem[2];
        orderItems[0] = new MaterialItem(new Item(1, "name", BigDecimal.ONE), 1);
        orderItems[1] = new ServiceItem(new Item(1, "name", BigDecimal.ONE), 1);
        final Order order = new Order(orderItems);

        assertThat(order.getOrderTotal(BigDecimal.ZERO), is(new BigDecimal("2.00")));
        assertThat(order.getOrderTotal(new BigDecimal(0.5f)), is(new BigDecimal("2.50")));
        assertThat(order.getOrderTotal(BigDecimal.ONE), is(new BigDecimal("3.00")));
        assertThat(order.getOrderTotal(new BigDecimal(2.0f)), is(new BigDecimal("4.00")));
    }

    @Test
    public void getItemsReturnsAnAlphabeticallySortedByItemNameCollection() {
        final OrderItem[] orderItems = new OrderItem[7];
        orderItems[0] = new MaterialItem(new Item(1, "hamburger", BigDecimal.ZERO), 1);
        orderItems[1] = new MaterialItem(new Item(1, "french fries", BigDecimal.ZERO), 1);
        orderItems[2] = new MaterialItem(new Item(1, "coke", BigDecimal.ZERO), 1);
        orderItems[3] = new MaterialItem(new Item(1, "shake", BigDecimal.ZERO), 1);
        orderItems[4] = new MaterialItem(new Item(1, "Blast", BigDecimal.ZERO), 1);
        orderItems[5] = new MaterialItem(new Item(1, "burrito", BigDecimal.ZERO), 1);
        orderItems[6] = new MaterialItem(new Item(1, "Coney", BigDecimal.ZERO), 1);
        final Order order = new Order(orderItems);
        OrderItem[] sortedItems = new OrderItem[7];
        sortedItems = (OrderItem[]) order.getItems().toArray(sortedItems);

        assertThat(sortedItems[0].getItem().getName(), is("Blast"));
        assertThat(sortedItems[1].getItem().getName(), is("burrito"));
        assertThat(sortedItems[2].getItem().getName(), is("coke"));
        assertThat(sortedItems[3].getItem().getName(), is("Coney"));
        assertThat(sortedItems[4].getItem().getName(), is("french fries"));
        assertThat(sortedItems[5].getItem().getName(), is("hamburger"));
        assertThat(sortedItems[6].getItem().getName(), is("shake"));
    }
}
