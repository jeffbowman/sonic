package com.exam;

import java.math.BigDecimal;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class MaterialItemTest {

    @Test
    public void getCostShouldReturnCorrectlyCalculatedCostBasedOnTax() {
        final MaterialItem item = new MaterialItem(new Item(1, "name", BigDecimal.ONE), 1);
        assertThat(item.getCost(BigDecimal.ZERO), is(new BigDecimal("1")));
        assertThat(item.getCost(new BigDecimal(0.5f)), is(new BigDecimal("1.5")));
        assertThat(item.getCost(BigDecimal.ONE), is(new BigDecimal("2")));
        assertThat(item.getCost(new BigDecimal(2.0f)), is(new BigDecimal("3")));
    }

    @Test
    public void getCostShouldAccountForQuantity() {
        final MaterialItem item = new MaterialItem(new Item(1, "name", BigDecimal.ONE), 3);
        assertThat(item.getCost(BigDecimal.ZERO), is(new BigDecimal("3")));
        assertThat(item.getCost(BigDecimal.ONE), is(new BigDecimal("6")));
    }
}
