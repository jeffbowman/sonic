package com.exam;

import java.math.BigDecimal;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class ServiceItemTest {

    @Test
    public void getCostShouldReturnThePriceOfTheItem() {
        final ServiceItem serviceItem = new ServiceItem(new Item(1, "name", new BigDecimal(1.0f)), 1);
        assertThat(serviceItem.getCost(new BigDecimal(1.0f)), is(new BigDecimal("1")));
        assertThat(serviceItem.getCost(new BigDecimal(3.14159f)), is(new BigDecimal("1")));
    }

    @Test
    public void getCostShouldAccountForQuantity() {
        final ServiceItem item = new ServiceItem(new Item(1, "name", new BigDecimal(1.0f)), 3);
        assertThat(item.getCost(new BigDecimal(0.0f)), is(new BigDecimal("3")));
        assertThat(item.getCost(new BigDecimal(1.0f)), is(new BigDecimal("3")));
    }
}
